﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TestImageOperators;
using InterfaceImageOperators;
using OpenCvSharp;
using System.Linq.Expressions;
using OpenCvSharp.Extensions;
using ClassTask5RosendoB;

namespace WindowsFormsApp2
{
    
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        // List that stores all test functions callbacks
        List<Func<Mat,TestResults>> functions=new List<Func<Mat, TestResults>>();
        Mat img;

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            int i = comboBox1.SelectedIndex;
            // Execute function that corresponds to the one clicked
            TestResults results = functions[i].Invoke(img);

            // Show results 
            OriginalPictureBox.Image = BitmapConverter.ToBitmap(img);
            OpenCVPictureBox.Image = BitmapConverter.ToBitmap(results.CvMat);
            OwnPictureBox.Image = BitmapConverter.ToBitmap(results.OwnMat);
            DifferencesPictureBox.Image = BitmapConverter.ToBitmap(results.DiffMat);
            textBox1.Text = results.EqualImages.ToString();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            img = Cv2.ImRead(@"..\..\Images\testImage1.jpg");

            IImageOperators operators = new ImageOperationsRosendo();
            TestFunctions testFunctions = new TestFunctions(operators);

            // Fill list of functions using Reflection
            foreach(var function in testFunctions.GetType().GetMethods())
            {
                // Only include our test functions. Avoid Equals, GetType, etc
                if (function.Name.Contains("Test"))
                {
                    // Create delegate to function and add function name to comboBox
                    var func = Delegate.CreateDelegate(typeof(Func<Mat, TestResults>), testFunctions, function, false);
                    functions.Add((Func<Mat, TestResults>)func);
                    comboBox1.Items.Add(function.Name);
                }
            }
        }

    }

}
